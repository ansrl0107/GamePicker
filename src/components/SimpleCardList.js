import React, { Component } from 'react';
import './SimpleCardList.css';

import { Link } from 'react-router-dom';

class SimpleCardList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listWidth: 0
        }
    }
    componentDidMount = () => {
        this.handleWidth();
        window.addEventListener('resize',() => {
            this.handleWidth();
        })
    }
    handleWidth = () => {
        let width = window.innerWidth;
        if(width > 1024) {
            width -= 240;
        }
        width -= 80;
        this.setState({
            listWidth: Math.floor(width/140)*140
        })
    }
    render() {
        return (
            <div className='simple-card-list' ref='list' style={{width: this.state.listWidth}}>
                {this.props.list.map((data,index) => {
                    return (<SimpleCard key={index} id={data.id} title={data.title} imgLink={data.img_link} />);
                })}
            </div>
        );
    }
}

class SimpleCard extends Component {
    render() {
        return (
            <Link to={`/game/${this.props.id}/info`} className='simple-card'>
                <div className='simple-card-img' style={{backgroundImage: `url(${this.props.imgLink})`}}></div>
                <div className='simple-card-title'>{this.props.title}</div>
            </Link>
        );
    }
}
export default SimpleCardList;