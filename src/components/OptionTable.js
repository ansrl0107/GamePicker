import React, { Component } from 'react';
import './OptionTable.css';

class OptionTable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            index: 0,
            options: [
                {
                    name: '플랫폼',
                    list: [
                        'Windows','MacOS','IOS','Android','XBox'
                    ]
                },
                {
                    name: '옵션이름1',
                    list: [
                        'opt1','opt2','opt3'
                    ]
                },
                {
                    name: '옵션이름2',
                    list: [
                        'opt1','opt2','opt3'
                    ]
                },
                {
                    name: '옵션이름3',
                    list: [
                        'opt1','opt2','opt3'
                    ]
                },
                {
                    name: '옵션이름4',
                    list: [
                        'opt1','opt2','opt3'
                    ]
                }
            ],
            selected: []
        };
    }
    changeItems = (e) => {
        const value = e.currentTarget.dataset.id;
        Array.apply(null, this.refs.names.childNodes).map((div) => {
            return div.classList.remove('click');
        });
        e.currentTarget.classList.add('click');
        this.setState({
            index: value
        });
    }
    handleTags = (e) => {
        const option = this.state.options[this.state.index].name;
        const value = e.currentTarget.dataset.value;
        const list = this.state.selected.concat({option: option, value: value});
        const unique = list.filter((value, idx, list) => list.indexOf(value) === idx);
        this.setState({
            selected: unique
        });
        this.props.handle(list);
    }
    removeTags = (e) => {
        const target_index = Number(e.currentTarget.parentNode.dataset.id);
        var tmp = [...this.state.selected];
        tmp.splice(target_index,1);
        this.setState({
            selected: tmp
        });
        this.props.handle(tmp);    
    }
    render() {
        return (
            <div className='option-table'>
                <div className='option-name-list' ref='names'>
                    {this.state.options.map((data,index) => {
                        if(index===this.state.index) {
                            return (<div key={index} className='click' data-id={index} onClick={this.changeItems}>{data.name}</div>);
                        }
                        return (<div key={index} data-id={index} onClick={this.changeItems}>{data.name}</div>);
                    })}
                </div>
                <div className='select-option-item'>
                    {this.state.options[this.state.index].list.map((item,index) => {
                        return (
                            <div key={index} onClick={this.handleTags} data-value={item}>{item}</div>
                        ); 
                    })}
                </div>
                <div className='tag-list'>
                    {this.state.selected.map((data,index) => {
                        return (<Tag id={index} key={index} option={data.option} value={data.value} remove={this.removeTags}/>);
                    })}
                </div>
            </div>
        );
    }
}

class Tag extends Component {
    render() {
        return (
            <div className='tag' data-id={this.props.id}>
                <div>{this.props.option}</div>
                <div>{this.props.value}</div>
                <div onClick={this.props.remove}></div>
            </div>
        );
    }
}

export default OptionTable;