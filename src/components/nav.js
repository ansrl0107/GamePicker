import React, { Component } from 'react';
import Login from './Login';
import './nav.css';
import { Link } from 'react-router-dom';

class nav extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active_login : false
        };
    }
    handleLogin = () => {
        this.setState({
            active_login: !this.state.active_login
        });
        if(!this.state.active_login) {
            document.querySelector('.content').style.filter='blur(10px)';
        } else {
            document.querySelector('.content').style.filter='none';
        }
    }
    render() {
        function toggle() {
            document.querySelector('.nav').classList.toggle('toggle');
            document.querySelector('.content').classList.toggle('blur');
            document.querySelector('.menu-trigger').classList.toggle('active');
        }
        return (
            <React.Fragment>
                {this.state.active_login && <Login active={this.handleLogin}/>}
                <div className='nav'>
                    <Link to='/' className='logo'></Link>
                    <input className='nav-search' placeholder='게임을 검색해보세요'></input>
                    <ul className='menu'>
                        <Link to='/'>홈</Link>
                        <Link to='/game/search?'>게임 탐색</Link>
                        <Link to='/game/recommend'>게임 추천</Link>
                        <Link to='/talk/all'>커뮤니티</Link>
                        <Link to='/input'>게임 추가</Link>
                    </ul>
                    <div className='sign-box'>
                        <div className='sign-in' onClick={this.handleLogin}>로그인</div>
                    </div>
                </div>
                <div className='top-nav'>
                    <div className='menu-trigger' onClick={toggle}>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                    <div className='logo'>GAMEPICKER</div>
                </div>
            </React.Fragment>
        );
    }
}
export default nav;
