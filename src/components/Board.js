import React, { Component } from 'react';
import './Board.css';

class Board extends Component {
    constructor(props) {
        super(props);
        this.state = {
            articles: [
                {
                    title: '글 제목',
                    comment_cnt: '32',
                    category: '자유',
                    wdate: '2018-09-01 14:16',
                    writer: '작성자'
                },{
                    title: '글 제목',
                    comment_cnt: '32',
                    category: '자유',
                    wdate: '2018-09-01 14:16',
                    writer: '작성자'
                },{
                    title: '글 제목',
                    comment_cnt: '32',
                    category: '자유',
                    wdate: '2018-09-01 14:16',
                    writer: '작성자'
                },{
                    title: '글 제목',
                    comment_cnt: '32',
                    category: '자유',
                    wdate: '2018-09-01 14:16',
                    writer: '작성자'
                },{
                    title: '글 제목',
                    comment_cnt: '32',
                    category: '자유',
                    wdate: '2018-09-01 14:16',
                    writer: '작성자'
                },{
                    title: '글 제목',
                    comment_cnt: '32',
                    category: '자유',
                    wdate: '2018-09-01 14:16',
                    writer: '작성자'
                },
                {
                    title: '글 제목',
                    comment_cnt: '32',
                    category: '자유',
                    wdate: '2018-09-01 14:16',
                    writer: '작성자'
                },
                {
                    title: '글 제목',
                    comment_cnt: '32',
                    category: '자유',
                    wdate: '2018-09-01 14:16',
                    writer: '작성자'
                },
                {
                    title: '글 제목',
                    comment_cnt: '32',
                    category: '자유',
                    wdate: '2018-09-01 14:16',
                    writer: '작성자'
                },
                {
                    title: '글 제목',
                    comment_cnt: '32',
                    category: '자유',
                    wdate: '2018-09-01 14:16',
                    writer: '작성자'
                }
            ]
        };
    } 
    render() {
        return (
            <React.Fragment>
                <div className='board'>
                    <div className='board-header'>
                        <div className='board-category'>자유</div>
                        <div className='board-sort'>
                            <div className='popular'>인기</div>
                            <div className='recent'>최신</div>
                            <div className='top'>TOP</div>
                            <div className='10'>10추</div>
                        </div>
                        <div className='board-search'>
                            <select>
                                <option>제목</option>
                                <option>내용</option>
                                <option>작성자</option>
                            </select>
                            <input placeholder='게임 이름을 입력해주세요'></input>
                            <div>검색</div>
                        </div>
                    </div>
                    <div className='board-content'>
                    {this.state.articles.map(data => {
                        return (<Article data={data}/>);
                    })}
                    </div>
                    <div className='board-footer'>
                        <div>
                            <div className='prev'>이전</div>
                            <ul className='pages'>
                                <li>1</li>
                                <li>2</li>
                                <li>3</li>
                                <li>4</li>
                                <li>5</li>
                                <li>6</li>
                                <li>7</li>
                            </ul>
                            <div className='next'>다음</div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

class Article extends Component {
    render() {
        const { title, comment_cnt, category, wdate, writer } = this.props.data;
        return (
            <div className='article'>
                <div>
                    <div className='title'>{title}</div>
                    <div className='comment-cnt'>[{comment_cnt}]</div>
                </div>
                <div>
                    <div className='category'>{category}</div>
                    <div className='wdate'>{wdate}</div>
                    <div className='writer'>{writer}</div>
                </div>
            </div>
        );
    }
}

export default Board;