import React, { Component } from 'react';
import './FullSlider.css';

import Indicator from './Indicator';

class FullSlider extends Component {
    constructor(props) {
        super(props);
        this.state = {
            index: 0,
            pushAmount: 0
        }
    }
    prev = () => {
        const { index } = this.state;
        this.setState({index: index===0?this.props.list.length-1:index-1});
    }
    next = () => {
        const { index } = this.state;
        this.setState({index: index===this.props.list.length-1?0:index+1})
    }
    handleIndex = (index) => {
        this.setState({index: index});
    }
    render() {
        return (
            <div className='full-slider'>
                <ul style={{width: 100*this.props.list.length + '%', marginLeft: this.state.index*-100 + '%'}}>
                    {this.props.list.map((data, index) => {
                        return (<Slide
                            key = {index}
                            width = {100/this.props.list.length + '%'}
                            title = {data.title}
                            content = {data.content}
                            imgLink = {data.imgLink}
                        />);
                    })}
                </ul>
                <div className='full-slider-prev' onClick={this.prev}></div>
                <div className='full-slider-next' onClick={this.next}></div>
                <Indicator 
                    len = {this.props.list.length}
                    cur = {this.state.index}
                    handle = {this.handleIndex}
                />
            </div>
        );
    }
}

class Slide extends Component {
    render() {
        return (
            <li className='slide' style={{backgroundImage: `url(${this.props.imgLink})`, width: this.props.width}}>
                <div className='slide-meta'>
                    <div className='slide-title'>{this.props.title}</div>
                    <div className='slide-content'>{this.props.content}</div>
                </div>
            </li>
        );
    }
}
export default FullSlider;