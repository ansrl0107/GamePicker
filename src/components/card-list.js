import React, { Component } from 'react';
import './card-list.css';
import { Link } from 'react-router-dom';

import StarRating from './StarRating'

class CardList extends Component {
	render() {
		console.log(this.props.list);
		
		return (
			<div className='card-list'>
				{this.props.list.map((data) => {
					const { id, title, img_link } = data;					
					return (<Card img_link={img_link} title={title} key={id} id={id}/>)
				})}
			</div>
		);
	}
}
class Card extends Component {
	render() {
		return (
		<Link to={`game/${this.props.id}/info`} className='card' data-id={this.props.id} >
			<div className='card-image' style={{backgroundImage: `url(${this.props.img_link})`}}>
				<div className='card-meta'>
					<div className='rating' ><StarRating target={this.props.id}/></div>
					<div className='title'>{this.props.title}</div>
					<div className='rate'>3.1</div>
				</div>
			</div>
		</Link>
		);
	}
}

export default CardList;
