import React, { Component } from 'react';
import './Slider.css';

import Indicator from './Indicator';

class Slider extends Component {
    static defaultProps = {
        width: 500
    }
    constructor(props) {
        super(props);
        this.state = {
            index: 0,
            pageCount: 0,
            width: 0
        }
    }
    static getDerivedStateFromProps = (nextProps, prevState) => {
        const w =   window.innerWidth>1024?
                    window.innerWidth-280:
                    window.innerWidth;
        const pageCount = Math.ceil(nextProps.list.length/Math.floor(w/170));
        return {
            width: w,
            pageCount: pageCount
        }
    }
    componentDidMount = () => {
        window.addEventListener('resize',this.setSize)
    }
    setSize = () => {
        const w =   window.innerWidth>1024?
                    window.innerWidth-280:
                    window.innerWidth;
        //card width + card margin = 170px
        if(this.state.index > this.state.pageCount-1) {
            this.setState({
                index: this.state.pageCount-1
            })
        }
        this.setState({
            width: w,
            pageCount: Math.ceil(this.props.list.length/Math.floor(this.state.width/170))
        });
    }
    handleIndex = (index) => {
        this.setState({index: index});
    }
    prev = () => {
        const { index, pageCount } = this.state;
        this.setState({index: index===0?pageCount-1:Number(index)-1});
    }
    next = () => {
        const { index, pageCount } = this.state;
        this.setState({index: index===pageCount-1?0:Number(index)+1});
    }
    calcPushAmount = () => {
        const unit_amount =  Math.floor(this.state.width/170)*170;
        let amount = unit_amount*this.state.index;
        if(this.state.index === this.state.pageCount-1) {
            amount = this.props.list.length*170-(this.state.width-20)
        }
        return -1*amount+'px';
    }
    render() {
        return (
            <div className='slider' ref='slider' style={{width: this.state.width+'px'}}>
                <div className='slider-title'>{this.props.title}</div>
                <div className='slider-btn'>
                    <div className='slider-prev' onClick={this.prev}></div>
                    <div className='slider-next' onClick={this.next}></div>
                </div>

                <ul ref='slider' style={{marginLeft: this.calcPushAmount()}}>
                    {this.props.list.map((data,index) => {
                        return (
                            <SmallCard
                                key={index}
                                title={data.title}
                                imgLink={data.imgLink}
                            />
                        )
                    })}
                </ul>
                <Indicator 
                    len = {this.state.pageCount}
                    cur = {this.state.index}
                    handle = {this.handleIndex}
                />
            </div>
        );
    }
}
class SmallCard extends Component {
    render() {
        return (
            <li className='small-card'>
                <div className='small-card-img' style={{backgroundImage: `url(${this.props.imgLink})`}}></div>
                <div className='small-card-meta'>
                    <div className='small-card-title'>{this.props.title}</div>
                </div>
            </li>
        );
    }
}
export default Slider;