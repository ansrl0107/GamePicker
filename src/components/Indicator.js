import React, { Component }  from 'react';
import './Indicator.css';

class Indicator extends Component {

    handleIndex = (e) => {
        const target = e.currentTarget.dataset.id;
        Array.prototype.slice.call(this.refs.indicator.childNodes).map((node) => {
            node.classList.remove('current');
            return true;
        });
        e.currentTarget.classList.add('current');
        this.props.handle(Number(target));
    }
    render() {
        const ret = [];
        for(let i=0;i<this.props.len;i++) {
            if(this.props.cur === i) {
                ret.push(<div key={i} data-id={i} onClick={this.handleIndex} className='current'></div>)
            } else {
                ret.push(<div key={i} data-id={i} onClick={this.handleIndex}></div>)
            }
        }
        return (
            <div className='indicator' ref='indicator'>
                {ret}
            </div>
        );
    }
}
export default Indicator;