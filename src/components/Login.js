import React, { Component } from 'react';
import './Login.css';

class Login extends Component {
    state = {
        isRegister: false,
        mode: true,
        active: false,
        email: '',
        password: ''
    }
    handleMode = (e) => {
        const mode = e.currentTarget.dataset.value;
        if(mode==='login') {
            this.refs.Mode.childNodes[0].classList.add('select');
            this.refs.Mode.childNodes[1].classList.remove('select');
            this.setState({mode: true});
        } else {
            this.refs.Mode.childNodes[0].classList.remove('select');
            this.refs.Mode.childNodes[1].classList.add('select');
            this.setState({mode: false});
        }
    }
    handleActive = () => {
        this.props.active();
    }
    login = () => {
        fetch('http://localhost:3333/users/login',{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: this.state.email,
                password: this.state.password
            })
        }).then(res => res.json())
        .then(data => console.log(data));
       
    }
    updateInput = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    render() {
        return (
            <div className='login-box'>
                <div className='exit-btn' onClick={this.handleActive}></div>
                <div className='login-menu' ref='Mode'>
                    <div className='select' onClick={this.handleMode} data-value='login'>로그인</div>
                    <div onClick={this.handleMode} data-value='sign-up'>회원가입</div>
                </div>
                {!this.state.mode && <input className='nickname' placeholder='닉네임을 입력해주세요.'></input>}
                <input className='email' name = 'email' placeholder='이메일을 입력해주세요.' value={this.state.email} onChange={this.updateInput}></input>
                <input className='password' name = 'password' placeholder='비밀번호를 입력해주세요' value={this.state.password} onChange={this.updateInput}></input>
                <div className='btns'>
                    <div className='facebook' >페이스북으로 로그인</div>
                    <div className='' onClick={this.login}>다음</div>
                </div>
            </div>
        );
    }
}
export default Login;