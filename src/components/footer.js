import React, { Component } from 'react';
import './footer.css';

class footer extends Component {
    render() {
        return (
            <div className='footer'>
                <div>
                    <div>서비스 이용약관</div>
                    <div>개인정보 처리방침</div>
                </div>
                <div>
                    <div>사업자 등록 번호(123-45-6789)</div>
                    <div>고객센터 cs@gamepicker.com</div>
                </div>
                <div>
                    <div>© 2018 GamePicker Inc. All Rights Reserved</div>
                </div>
            </div>
        );
    }
}
export default footer;