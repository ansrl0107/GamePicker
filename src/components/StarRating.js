import React, { Component } from 'react';
import './StarRating.css';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStar } from '@fortawesome/free-regular-svg-icons';
import { faStar as starFill,faStarHalfAlt} from '@fortawesome/free-solid-svg-icons';

class StarRating extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rate: 0
        }
    }
    setRate = (e) => {
        let id = Number(e.currentTarget.dataset.id);
        if(!id)
            id=0;
        this.setState({
            rate: id
        });
    }
    click = (e) => {
        e.preventDefault();
        alert(this.props.target + ':' + this.state.rate)
    }
    render() {
        const list = [];
        for(let i=1;i<=10;i++) {
            list.push(<div data-id={i/2} onMouseEnter={this.setRate} onClick={this.click}></div>);
        }
        const stars = [];
        let i=this.state.rate;
        while(i>=1) {
            stars.push(<FontAwesomeIcon icon={starFill}/>)
            i--;
        }
        if(i===0.5) {
            stars.push(<FontAwesomeIcon icon={faStarHalfAlt}/>)
        }
        for(let i=0;i<Math.floor(5-this.state.rate);i++) {
            stars.push(<FontAwesomeIcon icon={faStar}/>)
        }
        return (
            <div className='star-rating' onMouseLeave={this.setRate}>
                <div>{stars}</div>
                <div>{list}</div>
                
            </div>
        );
    }
}
export default StarRating;