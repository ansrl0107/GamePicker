import React, { Component } from 'react';
import OptionTable from '../components/OptionTable';
import './GameSearch.css';
import SimpleCardList from '../components/SimpleCardList';
import { Link } from 'react-router-dom';

class GameSearch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            filter: [],
            list: [],
            query: ''
        }
    }
    handleFilter = (list) => {
        this.setState({
            filter: list
        })
    }
    searchGames = (e) => {
        this.setState({
            query: e.target.value
        })
        if(!e.target.value) {
            this.setState({
                list: []
            })
        } else {
            fetch(`http://localhost:3333/game/search/${e.target.value}`)
            .then(res => res.json())
            .then(json => this.setState({
                list: json
            }));
        }
    }
    render() {
        return (
            <React.Fragment>
                <input className='search' placeholder='게임을 검색해보세요' value={this.state.query} onChange={this.searchGames}></input>
                <OptionTable handle={this.handleFilter}/>
                {!this.state.list.length &&
                    <div className='empty-holder'>
                        <div>찾는 게임이 아직 없는거같아요 ㅠㅠ 제일먼저 등록해볼까요?</div>
                        <Link to='/input'>등록하러가기</Link>
                    </div>
                }
                <SimpleCardList list={this.state.list}/>
            </React.Fragment>
        );  
    }
}
export default GameSearch;