import React, { Component } from 'react';
import './user.css';

import Slider from '../components/Slider';

class user extends Component {
    constructor(props) {
        super(props);
        this.state = {
            list: [],
            comment: [],
            post: []
        }
    }
    componentDidMount = () => {
        this.getUserGame();
    }
    getUserGame = () => {
        const list = []
        /* ajax로 값을 읽어옴 */
        for(let i=0;i<10;i++) {
            list.push({
                title: '제목 ' + i,
                imgLink: 'https://t1.daumcdn.net/thumb/R1280x0/?fname=http://t1.daumcdn.net/brunch/service/user/Aaa/image/j-QCpiSBCtIgpdOpDtBI6lTJsxU.jpg'
            })
        }
        /* ------------------ */
        this.setState({
            list: list
        })
    }
    getUserPost = () => {

    }
    getUserComment = () => {

    }
    render() {
      return (
        <div className='user'>
            <div className='user-bg-img'></div>
            <div className='user-meta'>
                <div className='user-img'></div>
                <div className='user-name'>카사딘</div>
            </div>
            <Slider title='내 게임' list={this.state.list} />
            <div className='my-post'>
                <div className='my-articles'></div>
                <div className='my-comments'></div>
            </div>
        </div>
      );
    }
  }
  
  export default user;
  