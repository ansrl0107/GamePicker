import React, { Component } from 'react';
import FullSlider from '../components/FullSlider';
import CardList from '../components/card-list';
import './home.css';


class Home extends Component {
	constructor(props) {
		super(props);
		this.state = {
			list: [],
			slideList: []
		}
	}
	componentDidMount = () => {
		this.getCardList();
		this.getSlideList();
	}

	getSlideList = () => {
		/* ajax로 값 읽어오기 */
		const list = [];
		for(let i=0;i<10;i++) {
			list.push({
				title: i + "번째 제목입니다",
				content: "내용을 적어야하는데 무엇을 적어야할까요??",
				imgLink: "http://upload.inven.co.kr/upload/2014/11/29/bbs/i2931341807.jpg"
			})
		}		
		/* ------ */
		this.setState({
			slideList: list
		});
	}
	
	getCardList = () => {
		fetch('http://localhost:3333/game/recommend', {
			headers: {
				"Content-Type": "application/json;charset=UTF-8"  
			}
		}).then(res => res.json())
			.then(json => {
				console.log(json);
				
				this.setState({list: json});
			});
	}
    render() {
      	return (
        	<React.Fragment>
				<FullSlider list={this.state.slideList}/>
				<CardList list={this.state.list}/>
        	</React.Fragment>
      );
    }
  }
  
  export default Home;
  