import React, { Component } from 'react';
import './inputForm.css';

class InputForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            developer: '',
            publisher: '',
            age_rate: '',
            summary: '',
            img_link: '',
            video_link: '',
            tags: [],
            platforms: []
        }
    }
    updateInputState = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    createGames = () => {
        const json = {
            title: this.state.title,
            developer: this.state.developer,
            publisher: this.state.publisher,
            age_rate: this.state.ageRate,
            summary: this.state.summary,
            img_link: this.state.imgLink,
            video_link: this.state.videoLink
        };
        fetch('http://localhost:3333/game', {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(json)
        });
    }
    render() {
        const tagList = [
            '액션','호러'
        ];
        const platformList = ['windows','macOS','android','ios','xbox'];
        return (
            <div className='input-form'>
                <h3>게임이름</h3><input name='title' value={this.state.title} onChange={this.updateInputState}/>
                <h3>개발사</h3><input name='developer' value={this.state.developer} onChange={this.updateInputState}/>
                <h3>배급사</h3><input name='publisher' value={this.state.publisher} onChange={this.updateInputState}/>
                <h3>이용가</h3><input name='age_rate' value={this.state.age_rate} onChange={this.updateInputState}/>
                <h3>요약</h3><textarea name='summary' value={this.state.summary} onChange={this.updateInputState}></textarea>
                <h3>이미지 링크</h3><input name='img_link' value={this.state.img_link} onChange={this.updateInputState}/>
                <h3>비디오 링크</h3><input name='video_link' value={this.state.video_link} onChange={this.updateInputState}/>
                <h3>태그</h3>
                <ul ref='tag'>
                    {tagList.map((data,index) => {
                        return (<li key={index}><input type='checkbox'/><div>{data}</div></li>)
                    })}
                </ul>
                <h3>플랫폼</h3>
                <ul ref='platform'>
                    {platformList.map((data,index) => {
                        return (<li key={index}><input type='checkbox'/><div>{data}</div></li>)
                    })}
                </ul>
                <div className='btn' onClick={this.createGames}>제출</div>
            </div>
        );
    }
}
export default InputForm;