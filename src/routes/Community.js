import React, { Component } from 'react';
import Board from '../components/Board';
import './Community.css';

class Community extends Component{
    render() {
        return (
            <React.Fragment>
                <div className='board-img'></div>
                <Board />
            </React.Fragment>
        );
    }
}
export default Community;