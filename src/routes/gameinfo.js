import React, { Component } from 'react';
import './gameinfo.css';

class gameinfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {},
            //imgLink, title, summary ,genre, developer, publisher, ageRating
            comments: []
            //writer, content
        }
    }
    componentDidMount = () => {
        this.getGameInfo();
        this.getComment();
    }
    getGameInfo = () => {
        const id = this.props.match.params.gamename;
        fetch(`http://localhost:3333/game/${id}/info`)
            .then(res => res.json())
            .then(json => this.setState({
                data: json[0]
            })
        );
    }
    getComment = () => {
        let comments = [];
        /* ajax */
        for(let i=0;i<10;i++) {
            comments.push({
                writer: '작성자'+i,
                content: '내용'+i
            })
        }
        /* ---- */
        this.setState({comments: comments});
    }

    render() {        
        const { title, developer, publisher, age_rate, summary, img_link, video_link } = this.state.data;
        const tags = ['미정'];
        const platform = ['미정'];
        return (
            <React.Fragment>
                <div className='game-img' style={{backgroundImage: `url(${img_link})`}}></div>
                <div className='game-title'>{title}</div>
                <h2>요약</h2>
                <div className='game-summary'>{summary}</div>
                <h2>개요</h2>
                <div className='game-meta'>
                    <ul>
                        <li><div>장르</div><div>{tags}</div></li>
                        <li><div>개발사</div><div>{developer}</div></li>
                        <li><div>이용 등급</div><div>{age_rate}</div></li>
                        <li><div>퍼블리셔</div><div>{publisher}</div></li>
                        <li><div>플랫폼</div><div>{platform}</div></li>
                    </ul>
                </div>
                <h2>한줄평</h2>
                <div className='comment-post'>
                    <input placeholder='한줄 평을 적어주세요.'></input>
                    <div>등록</div>
                </div>
                <div className='comment-list'>
                    {this.state.comments.map((data,index) => {
                        return (<Comment writer={data.writer} content={data.content} key={index}/>)
                    })}
                </div>
            </React.Fragment>
        );
    }
}

class Comment extends Component {
    render() {
        return (
            <div className='comment'>
                <div className='comment-writer'>{this.props.writer}</div>
                <div className='comment-content'>{this.props.content}</div>
            </div>
        );
    }
}
export default gameinfo;