import React,{ Component } from 'react';
import './GameRecommend.css';

import Slider from '../components/Slider';

class GameRecommend extends Component {
    constructor(props) {
        super(props);
        this.state = {
            list: []
        }
    }
    componentDidMount = () => {
        this.readGameList();
    }
    readGameList = () => {
        const list = []
        /* ajax로 값을 읽어옴 */
        for(let i=0;i<10;i++) {
            list.push({
                title: '제목 ' + i,
                imgLink: 'https://t1.daumcdn.net/thumb/R1280x0/?fname=http://t1.daumcdn.net/brunch/service/user/Aaa/image/j-QCpiSBCtIgpdOpDtBI6lTJsxU.jpg'
            })
        }
        /* ------------------ */
        this.setState({list: list});
    }
    render() {
        return (
            <React.Fragment>
                <Slider title='인기게임' list={this.state.list}/>
                <Slider title='이 주의 게임' list={this.state.list}/>
                <Slider title='이 달의 게임' list={this.state.list}/>
            </React.Fragment>
        );
    }
}
export default GameRecommend;