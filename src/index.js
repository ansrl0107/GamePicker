import React from 'react';
import ReactDOM from 'react-dom';
import Nav from './components/nav';
import Footer from './components/footer';
import './index.css';

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Home from './routes/home';
import User from './routes/user';
import GameInfo from './routes/gameinfo';
import GameSearch from './routes/GameSearch';
import GameRecommend from './routes/GameRecommend';
import Community from './routes/Community';
import InputForm from './routes/inputForm';
import NotFound from './routes/404';

ReactDOM.render(
    <React.Fragment>
        <Router>
            <React.Fragment>
                <Nav />
                <div className='content'>
                    <Switch>
                        <Route exact path='/' component={Home} />
                        <Route path='/user/:nickname' component={User} />
                        <Route path='/game/:gamename/info' component={GameInfo} />
                        <Route path='/game/search' component={GameSearch} />
                        <Route path='/game/recommend' component={GameRecommend}/>
                        <Route path='/talk/:type' component={Community} />
                        <Route path='/input' component={InputForm} />
                        <Route component={NotFound} />
                    </Switch>
                    <Footer />
                </div>
            </React.Fragment>
        </Router>
    </React.Fragment>
, document.getElementById('root'));